import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
} from 'react-native';
import {COLORS, FONTS, images, SIZES} from '../constants';
import LinearGradient from 'react-native-linear-gradient';
import {CustomButton} from '../components';

const Login = ({navigation}) => {
  const renderHeader = () => {
    return (
      <View
        style={{
          height: SIZES.height > 700 ? '65%' : '60%',
        }}>
        <ImageBackground
          style={{
            flex: 1,
            justifyContent: 'flex-end',
          }}
          resizeMode="cover"
          source={images.loginBackground}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 0, y: 1}}
            colors={[COLORS.transparent, COLORS.black]}
            style={{
              height: 200,
              justifyContent: 'flex-end',
              paddingHorizontal: SIZES.padding,
            }}>
            <Text
              style={{
                width: '80%',
                color: COLORS.white,
                lineHeight: 45,
                ...FONTS.largeTitle,
              }}>
              Cooking a Delicious Food Easily
            </Text>
          </LinearGradient>
        </ImageBackground>
      </View>
    );
  };

  const renderDetailsAndButton = () => {
    return (
      <View
        style={{
          flex: 1,
          paddingHorizontal: SIZES.padding,
        }}>
        <Text
          style={{
            marginTop: SIZES.radius,
            width: '70%',
            color: COLORS.gray,
            ...FONTS.body3,
          }}>
          Discover more than 1200 food recipes in your hands and cooking it
          easily!
        </Text>
        {/* Buttons */}
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            // marginBottom: 20,
          }}>
          <CustomButton
            text="Login"
            containerStyle={{
              paddingVertical: 18,
              borderRadius: 20,
            }}
            colors={[COLORS.darkGreen, COLORS.lime]}
            onPress={() => navigation.replace('Home')}
          />
          <CustomButton
            text="Sign Up"
            containerStyle={{
              marginTop: SIZES.radius,
              paddingVertical: 18,
              borderRadius: 20,
              borderWidth: 1,
              borderColor: COLORS.darkLime,
            }}
            colors={[]}
            onPress={() => navigation.replace('Home')}
          />
        </View>
      </View>
    );
  };
  return (
    <View style={{flex: 1, backgroundColor: COLORS.black}}>
      <StatusBar barStyle="light-content" />

      {/* Header */}
      {renderHeader()}

      {/* Detail */}
      {renderDetailsAndButton()}
    </View>
  );
};

export default Login;
