import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  Image,
  TextInput,
} from 'react-native';
import {COLORS, dummyData, FONTS, icons, images, SIZES} from '../constants';
import {CategoryCard} from '../components';
import {TredingCard} from '../components';

const Home = ({navigation}) => {
  const renderHeader = () => {
    return (
      <View
        style={{
          flex: 1,
          marginHorizontal: 20,
          flexDirection: 'row',
          alignItems: 'center',
          height: 80,
        }}>
        <View style={{flex: 1}}>
          <Text
            style={{
              color: COLORS.darkGreen,
              ...FONTS.h2,
            }}>
            Hello Chris,
          </Text>
          <Text
            style={{
              color: COLORS.gray,
              ...FONTS.body4,
            }}>
            What you want to cook today~?
          </Text>
        </View>
        <TouchableOpacity>
          <Image
            source={images.profile_2}
            style={{
              height: 40,
              width: 40,
              borderRadius: 40,
            }}
          />
        </TouchableOpacity>
        {/* <Image /> */}
      </View>
    );
  };

  const renderSearchBar = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 50,
          backgroundColor: COLORS.lightGray,
          marginHorizontal: 20,
          paddingHorizontal: 20,
          borderRadius: SIZES.radius,
          alignItems: 'center',
        }}>
        <Image
          source={icons.search}
          style={{
            resizeMode: 'contain',
            width: 20,
            height: 20,

            tintColor: COLORS.gray,
          }}
        />
        <TextInput
          style={{
            width: '88%',
            marginLeft: SIZES.radius,
            ...FONTS.body3,
          }}
          placeholder="Search Recipe"
          placeholderTextColor={COLORS.gray}
        />
      </View>
    );
  };

  const renderBanner = () => {
    return (
      <View
        style={{
          marginTop: 20,
          marginHorizontal: 20,
          padding: 20,
          height: 100,
          backgroundColor: COLORS.lightGreen,
          borderRadius: 10,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Image
          source={images.recipe}
          style={{
            width: 80,
            height: 80,
          }}
        />
        <View
          style={{
            marginLeft: 20,
            flex: 1,
          }}>
          <Text
            style={{
              ...FONTS.body4,
              width: '80%',
            }}>
            You have 12 recipes that you haven't tried yet.
          </Text>
          <TouchableOpacity>
            <Text
              style={{
                marginTop: 4,
                color: COLORS.darkGreen,
                textDecorationLine: 'underline',
                ...FONTS.h4,
              }}>
              See Recipes
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const renderTrending = () => {
    return (
      <View
        style={{
          marginTop: 16,
        }}>
        <Text
          style={{
            marginHorizontal: 20,
            ...FONTS.h2,
          }}>
          Treding Recipe
        </Text>

        <FlatList
          data={dummyData.trendingRecipes}
          keyExtractor={item => item.id}
          showsHorizontalScrollIndicator={false}
          horizontal
          renderItem={({item, index}) => {
            return (
              <TredingCard
                item={item}
                containerStyle={{
                  marginLeft: index == 0 ? SIZES.padding : 0,
                }}
                onPress={() => navigation.navigate('Recipe', {recipe: item})}
              />
            );
          }}
        />
        <View
          style={{
            marginTop: 20,
            marginHorizontal: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              ...FONTS.h2,
            }}>
            Categories
          </Text>
          <Text
            style={{
              ...FONTS.body4,
              color: COLORS.gray,
            }}>
            View More
          </Text>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      <FlatList
        data={dummyData.categories}
        keyExtractor={item => item.id}
        keyboardDismissMode="on-drag"
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={
          <View>
            {/* Header */}
            {renderHeader()}
            {renderSearchBar()}
            {renderBanner()}
            {renderTrending()}
            {/* searchBar */}
            {/* banner */}
          </View>
        }
        renderItem={({item}) => {
          return (
            <CategoryCard
              title={item.name}
              image={item.image}
              serving={item.serving}
              duration={item.duration}
              onPress={() => navigation.navigate('Recipe', {recipe: item})}
              containerStyle={{marginHorizontal: 20}}
            />
          );
        }}
        ListFooterComponent={
          <View
            style={{
              marginBottom: 100,
            }}
          />
        }
      />
    </SafeAreaView>
  );
};

export default Home;
