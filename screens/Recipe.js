import React, {useRef} from 'react';
import {
  View,
  Text,
  Animated,
  Platform,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import {BlurView} from '@react-native-community/blur';
import {COLORS, FONTS, icons, SIZES} from '../constants';
import {Viewers} from '../components';

const HEADER_HEIGHT = 350;

const Recipe = ({route, navigation}) => {
  const {recipe} = route.params;
  const scrollY = useRef(new Animated.Value(0)).current;

  const renderIngredientHeader = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 20,
          marginTop: SIZES.radius,
          marginBottom: SIZES.padding,
        }}>
        <Text
          style={{
            flex: 1,
            ...FONTS.h3,
          }}>
          Ingredients
        </Text>
        <Text
          style={{
            color: COLORS.lightGray2,
            ...FONTS.body4,
          }}>
          {recipe.ingredients.length} items
        </Text>
      </View>
    );
  };

  const renderRecipeInfo = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 130,
          width: SIZES.width,
          paddingHorizontal: 20,
          paddingVertical: 20,
          alignItems: 'center',
        }}>
        {/* Recipe Name */}
        <View
          style={{
            flex: 1.5,
            justifyContent: 'center',
          }}>
          <Text style={{...FONTS.h2}}>{recipe.name}</Text>
          <Text
            style={{
              marginTop: 5,
              color: COLORS.lightGray2,
              ...FONTS.body4,
            }}>
            {recipe.duration} | {recipe.serving} Serving
          </Text>
        </View>

        {/* Viewers */}
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
          }}>
          <Viewers viewersList={recipe.viewers} />
        </View>
      </View>
    );
  };

  const renderHeaderBar = () => {
    return (
      <View
        style={{
          zIndex: 999,
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          height: 100,
          flexDirection: 'row',
          alignItems: 'flex-end',
          paddingHorizontal: SIZES.padding,
          paddingBottom: 12,
          justifyContent: 'space-between',
        }}>
        <Animated.View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: COLORS.black,
            opacity: scrollY.interpolate({
              inputRange: [HEADER_HEIGHT - 200, HEADER_HEIGHT - 70],
              outputRange: [0, 1],
            }),
          }}
        />
        <Animated.View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'flex-end',
            paddingBottom: 10,
            opacity: scrollY.interpolate({
              inputRange: [HEADER_HEIGHT - 100, HEADER_HEIGHT - 50],
              outputRange: [0, 1],
            }),
            transform: [
              {
                translateY: scrollY.interpolate({
                  inputRange: [HEADER_HEIGHT - 280, HEADER_HEIGHT - 50],
                  outputRange: [50, 0],
                  extrapolate: 'clamp',
                }),
              },
            ],
          }}>
          <Text style={{color: COLORS.lightGray2, ...FONTS.body4}}>
            Recipe by:
          </Text>
          <Text style={{color: COLORS.white2, ...FONTS.h3}}>
            {recipe.author?.name}
          </Text>
        </Animated.View>
        <TouchableOpacity
          style={{
            width: 36,
            height: 36,
            borderRadius: 20,
            borderWidth: 1,
            borderColor: COLORS.white,
            backgroundColor: COLORS.transparentBlack5,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => navigation.goBack()}>
          <Image
            source={icons.back}
            style={{
              width: 16,
              height: 16,
              tintColor: COLORS.white,
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width: 36,
            height: 36,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => console.log('add book mark')}>
          <Image
            source={recipe.isBookmark ? icons.bookmarkFilled : icons.bookmark}
            style={{
              width: 26,
              height: 26,
              tintColor: COLORS.darkGreen,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const RecipeContainer = () => {
    if (Platform.OS === 'ios') {
      return (
        <BlurView
          style={{
            flex: 1,
            borderRadius: SIZES.radius,
          }}
          blurType="dark">
          {RecipeContainerDetails()}
        </BlurView>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.transparentBlack9,
          }}>
          {RecipeContainerDetails()}
        </View>
      );
    }
  };

  const RecipeContainerDetails = () => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          marginHorizontal: 20,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{marginRight: 20}}>
            <Image
              source={recipe.author.profilePic}
              style={{
                width: 40,
                height: 40,
                resizeMode: 'cover',
                borderRadius: 20,
              }}
            />
          </View>
          <View>
            <Text
              style={{
                color: COLORS.gray,
                ...FONTS.body4,
              }}>
              Recipe by:
            </Text>
            <Text style={{color: COLORS.white2, ...FONTS.h3}}>
              {recipe.author.name}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          style={{
            width: 30,
            height: 30,
            color: COLORS.lightGreen1,
            borderWidth: 1,
            borderColor: COLORS.white2,
            borderRadius: 5,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={icons.rightArrow}
            style={{tintColor: COLORS.white2, width: 15, height: 15}}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderRecipeHeader = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          overflow: 'hidden',
          marginTop: -1000,
          paddingTop: 1000,
        }}>
        <Animated.Image
          source={recipe.image}
          style={{
            resizeMode: 'contain',
            width: '200%',
            height: HEADER_HEIGHT,
            transform: [
              {
                translateY: scrollY.interpolate({
                  inputRange: [-HEADER_HEIGHT, 0, HEADER_HEIGHT],
                  outputRange: [-HEADER_HEIGHT / 2, 0, HEADER_HEIGHT * 0.75],
                }),
              },
              {
                scale: scrollY.interpolate({
                  inputRange: [-HEADER_HEIGHT, 0, HEADER_HEIGHT],
                  outputRange: [2, 1, 0.75],
                }),
              },
            ],
          }}
        />
        <Animated.View
          style={{
            position: 'absolute',
            left: 30,
            right: 30,
            bottom: 10,
            height: 80,
            transform: [
              {
                translateY: scrollY.interpolate({
                  inputRange: [0, 150, 230],
                  outputRange: [0, 0, 100],
                  extrapolate: 'clamp',
                }),
              },
            ],
          }}>
          <RecipeContainer item={recipe} />
        </Animated.View>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      <Animated.FlatList
        data={recipe?.ingredients}
        keyExtractor={item => item.id}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={
          <View>
            {renderRecipeHeader()}
            {renderRecipeInfo()}
            {renderIngredientHeader()}
          </View>
        }
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: scrollY}}}],
          {useNativeDriver: true},
        )}
        renderItem={({item}) => (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              paddingHorizontal: 20,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',

                marginVertical: 5,
                flex: 1,
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLORS.lightGray,
                  borderRadius: SIZES.radius,
                }}>
                <Image
                  source={item.icon}
                  style={{
                    width: 32,
                    height: 32,
                  }}
                />
              </View>

              <View
                style={{
                  marginLeft: 10,
                  width: '75%',
                }}>
                <Text style={{...FONTS.h3}}>{item.description}</Text>
              </View>
            </View>
            <View>
              <Text style={{...FONTS.body3}}>{item.quantity}</Text>
            </View>
          </View>
        )}
      />
      {renderHeaderBar()}
      <View style={{marginBottom: 50}} />
    </View>
  );
};

export default Recipe;
