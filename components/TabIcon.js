import React from 'react';
import {Text, View, Image} from 'react-native';
import {COLORS} from '../constants';

const TabIcon = ({focused, icon}) => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: 50,
      }}>
      <Image
        source={icon}
        style={{
          resizeMode: 'contain',
          height: 24,
          width: 24,
          tintColor: focused ? COLORS.darkGreen : COLORS.lightGreen1,
        }}
      />
      {focused && (
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 5,
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
            backgroundColor: COLORS.darkGreen,
          }}
        />
      )}
    </View>
  );
};

export default TabIcon;
