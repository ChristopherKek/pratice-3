import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  StyleSheet,
  Platform,
} from 'react-native';
import {COLORS, FONTS, icons, images, SIZES} from '../constants';
import {BlurView} from '@react-native-community/blur';

const RecipeContainer = ({item}) => {
  if (Platform.OS === 'ios') {
    return (
      <BlurView style={styles.recipeContainer} blurType="dark">
        <RecipeCardDetails item={item} />
      </BlurView>
    );
  } else {
    return (
      <View
        style={{
          ...styles.recipeContainer,
          backgroundColor: COLORS.transparentDarkGray,
        }}>
        <RecipeCardDetails item={item} />
      </View>
    );
  }
};

const RecipeCardDetails = ({item}) => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 10,
      }}>
      <View
        style={{
          width: '80%',
        }}>
        <Text style={{color: COLORS.white, flex: 1, ...FONTS.h3, fontSize: 18}}>
          {item.name}
        </Text>
        <Text style={{color: COLORS.lightGray, ...FONTS.body4}}>
          {item.duration} | {item.serving} serving
        </Text>
      </View>
      <View>
        <Image
          source={item.isBookmark ? icons.bookmarkFilled : icons.bookmark}
          style={{
            width: 20,
            height: 20,
            tintColor: COLORS.darkGreen,
          }}
        />
      </View>
    </View>
  );
};

function TredingCard({item, onPress, containerStyle}) {
  return (
    <TouchableOpacity
      style={{
        width: 250,
        height: 310,
        marginRight: 16,
        marginTop: 20,
        ...containerStyle,
      }}
      onPress={onPress}>
      <Image
        source={item.image}
        style={{
          resizeMode: 'cover',
          borderRadius: SIZES.radius,
          width: 250,
          height: 310,
        }}
      />
      <View
        style={{
          position: 'absolute',
          top: 15,
          left: 15,
          paddingHorizontal: SIZES.radius,
          paddingVertical: 5,
          borderRadius: SIZES.radius,
          backgroundColor: COLORS.transparentGray,
        }}>
        <Text
          style={{
            color: COLORS.white,
            ...FONTS.h4,
          }}>
          {item.category}
        </Text>
      </View>
      <RecipeContainer item={item} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  recipeContainer: {
    position: 'absolute',
    bottom: 10,
    left: 10,
    right: 10,
    height: 100,
    paddingVertical: SIZES.radius,
    paddingHorizontal: SIZES.base,
    borderRadius: SIZES.radius,
  },
});

export default TredingCard;
