import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {COLORS, FONTS} from '../constants';

const CustomButton = ({text, containerStyle, onPress, colors}) => {
  if (colors.length > 0) {
    return (
      <TouchableOpacity onPress={onPress}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={colors}
          style={{
            ...containerStyle,
          }}>
          <Text
            style={{
              textAlign: 'center',
              color: COLORS.white,
              ...FONTS.h3,
            }}>
            {text}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity style={{...containerStyle}} onPress={onPress}>
        <Text
          style={{
            textAlign: 'center',
            color: COLORS.white,
            ...FONTS.h3,
          }}>
          {text}
        </Text>
      </TouchableOpacity>
    );
  }
};

export default CustomButton;
