import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {COLORS, FONTS, SIZES} from '../constants';

const CategoryCard = ({
  title,
  image,
  duration,
  serving,
  onPress,
  containerStyle,
}) => {
  return (
    <TouchableOpacity
      style={{
        flex: 1,
        backgroundColor: COLORS.gray2,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: SIZES.radius,
        padding: 10,
        marginTop: 10,
        ...containerStyle,
      }}
      onPress={onPress}>
      <Image
        source={image}
        style={{
          width: 100,
          height: 100,
          borderRadius: SIZES.radius,
        }}
      />
      <View
        style={{
          paddingHorizontal: 20,
          width: '65%',
        }}>
        <Text
          style={{
            flex: 1,
            ...FONTS.h2,
          }}>
          {title}
        </Text>
        <Text
          style={{
            color: COLORS.gray,
            ...FONTS.body4,
          }}>
          {duration} | {serving} serving
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default CategoryCard;
